package com.example.demo.service.impl;

import static com.example.demo.model.Account.AccountType.CHECKING;
import static com.example.demo.model.Account.AccountType.PRIVATE_LOAN;
import static com.example.demo.model.Account.AccountType.SAVINGS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.demo.model.Account;
import com.example.demo.model.ReferenceAccount;
import com.example.demo.model.Transaction;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.ReferenceAccountRepository;
import com.example.demo.repository.TransactionRepository;
import com.example.demo.service.BankAccountService;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

class BankAccountServiceTest {

    private static final Account CHECKING_FIXTURE = Account.builder()
        .iban("CK12345678901234567890")
        .accountType(CHECKING)
        .balance(new BigDecimal("200.00"))
        .build();

    private static final Account SAVINGS_FIXTURE = Account.builder()
        .iban("SV23456789012345678901")
        .accountType(SAVINGS)
        .balance(new BigDecimal("100.00"))
        .build();

    private static final Account PRIVATE_LOAN_FIXTURE = Account.builder()
        .iban("PL34567890123456789012")
        .accountType(PRIVATE_LOAN)
        .build();

    private final AccountRepository accountRepositoryMock = mock(AccountRepository.class);
    private final TransactionRepository transactionRepository = mock(TransactionRepository.class);
    private final ReferenceAccountRepository referenceAccountRepository = mock(ReferenceAccountRepository.class);
    private final BankAccountService bankAccountService = new BankAccountServiceImpl(accountRepositoryMock,
        transactionRepository,
        referenceAccountRepository
    );

    @Captor
    private ArgumentCaptor<Account> accountCaptor;

    @Captor
    private ArgumentCaptor<ReferenceAccount> referenceAccountCaptor;

    @BeforeEach
    void before() {

        MockitoAnnotations.openMocks(this);

        when(accountRepositoryMock.findById("CK12345678901234567890")).thenReturn(Optional.of(CHECKING_FIXTURE));
        when(accountRepositoryMock.findById("SV23456789012345678901")).thenReturn(Optional.of(SAVINGS_FIXTURE));
        when(accountRepositoryMock.findById("PL34567890123456789012")).thenReturn(Optional.of(PRIVATE_LOAN_FIXTURE));
        when(accountRepositoryMock.existsById("SV23456789012345678901")).thenReturn(true);

        when(accountRepositoryMock.findAll()).thenReturn(Lists.newArrayList(CHECKING_FIXTURE,
            SAVINGS_FIXTURE,
            PRIVATE_LOAN_FIXTURE
        ));
        when(accountRepositoryMock.findAccountsByAccountTypeInOrderByCreatedAtDesc(Collections.singleton(CHECKING)))
            .thenReturn(Lists.newArrayList(CHECKING_FIXTURE));

        when(transactionRepository.findAllbyIbanOrderByTimestampDesc("CK12345678901234567890"))
            .thenReturn(Collections.singleton(
                Transaction.builder()
                    .target("CK12345678901234567890")
                    .amount(new BigDecimal("250.00"))
                    .build()
            ));

        when(referenceAccountRepository.findById("SV23456789012345678901")).thenReturn(Optional.of(
            new ReferenceAccount("SV23456789012345678901", "CK12345678901234567890")
        ));
    }

    @Test
    void testDepositForExistingAccount() {

        final Transaction transaction = Transaction.builder()
            .target("CK12345678901234567890")
            .amount(new BigDecimal("50.00"))
            .build();

        bankAccountService.deposit(transaction);

        verify(accountRepositoryMock, times(1)).findById("CK12345678901234567890");
        verify(accountRepositoryMock, times(1)).save(accountCaptor.capture());
        verify(transactionRepository, only()).save(transaction);

        assertThat(accountCaptor.getValue().getBalance()).isEqualByComparingTo(new BigDecimal("250.00"));
    }

    @Test
    void testDepositForNonExistingAccount() {

        final Transaction transaction = Transaction.builder()
            .target("CK67890123456789012345")
            .amount(new BigDecimal("50.00"))
            .build();

        assertThatThrownBy(() -> bankAccountService.deposit(transaction)).isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("Account not found");
    }

    @Test
    void testTransferFromCheckingToPrivateLoan() {

        final Transaction transaction = Transaction.builder()
            .source("CK12345678901234567890")
            .target("PL34567890123456789012")
            .amount(new BigDecimal("50.00"))
            .build();

        bankAccountService.transfer(transaction);

        verify(accountRepositoryMock, times(1)).findById("CK12345678901234567890");
        verify(accountRepositoryMock, times(1)).findById("PL34567890123456789012");
        verify(accountRepositoryMock, times(2)).save(accountCaptor.capture());
        verify(transactionRepository, only()).save(transaction);

        final Account updatedSource = accountCaptor.getAllValues().get(0);
        assertThat(updatedSource.getId()).isEqualTo("CK12345678901234567890");
        assertThat(updatedSource.getBalance()).isEqualByComparingTo(new BigDecimal("150.00"));

        final Account updatedTarget = accountCaptor.getAllValues().get(1);
        assertThat(updatedTarget.getId()).isEqualTo("PL34567890123456789012");
        assertThat(updatedTarget.getBalance()).isEqualByComparingTo(new BigDecimal("50.00"));
    }

    @Test
    void testTransferFromPrivateLoanToChecking() {

        final Transaction transaction = Transaction.builder()
            .source("PL34567890123456789012")
            .target("CK12345678901234567890")
            .amount(new BigDecimal("50.00"))
            .build();

        assertThatThrownBy(() -> bankAccountService.transfer(transaction)).isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("Withdrawal from private loan account is not possible");
    }

    @Test
    void testTransferWithoutAmount() {

        final Transaction transaction = Transaction.builder()
            .source("CK12345678901234567890")
            .target("PL34567890123456789012")
            .build();

        assertThatThrownBy(() -> bankAccountService.transfer(transaction)).isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("Amount needs to have a positive value");
    }

    @Test
    void testTransferFromSavingsToValidCheckingAccount() {

        final Transaction transaction = Transaction.builder()
            .source("SV23456789012345678901")
            .target("CK12345678901234567890")
            .amount(new BigDecimal("100.00"))
            .build();

        bankAccountService.transfer(transaction);

        verify(accountRepositoryMock, times(1)).findById("SV23456789012345678901");
        verify(accountRepositoryMock, times(1)).findById("CK12345678901234567890");
        verify(accountRepositoryMock, times(2)).save(accountCaptor.capture());
        verify(referenceAccountRepository, only()).findById("SV23456789012345678901");
        verify(transactionRepository, only()).save(transaction);

        final Account updatedSource = accountCaptor.getAllValues().get(0);
        assertThat(updatedSource.getId()).isEqualTo("SV23456789012345678901");
        assertThat(updatedSource.getBalance()).isEqualByComparingTo(BigDecimal.ZERO);

        final Account updatedTarget = accountCaptor.getAllValues().get(1);
        assertThat(updatedTarget.getId()).isEqualTo("CK12345678901234567890");
        assertThat(updatedTarget.getBalance()).isEqualByComparingTo(new BigDecimal("300.00"));
    }

    @Test
    void testTransferFromSavingsToInvalidAccount() {

        final Transaction transaction = Transaction.builder()
            .source("SV23456789012345678901")
            .target("PL34567890123456789012")
            .amount(new BigDecimal("50.00"))
            .build();

        assertThatThrownBy(() -> bankAccountService.transfer(transaction)).isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("Transfer from savings account is only possible to the designated reference account");
    }

    @Test
    void testTransferFromSavingsAboveLimit() {

        final Transaction transaction = Transaction.builder()
            .source("SV23456789012345678901")
            .target("CK12345678901234567890")
            .amount(new BigDecimal("100.01"))
            .build();

        assertThatThrownBy(() -> bankAccountService.transfer(transaction)).isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("Cannot withdraw more than the current balance from savings account");
    }

    @Test
    void testTransferForNonExistingAccount() {

        final Transaction transaction = Transaction.builder()
            .target("CK67890123456789012345")
            .source("SV23456789012345678901")
            .amount(new BigDecimal("50.00"))
            .build();

        assertThatThrownBy(() -> bankAccountService.transfer(transaction)).isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("Account not found");
    }

    @Test
    void testTransferBetweenIdenticalAccounts() {

        final Transaction transaction = Transaction.builder()
            .target("CK12345678901234567890")
            .source("CK12345678901234567890")
            .amount(new BigDecimal("50.00"))
            .build();

        assertThatThrownBy(() -> bankAccountService.transfer(transaction)).isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("Source and target account cannot be identical");
    }

    @Test
    void testGetBalanceForExistingAccount() {
        assertThat(bankAccountService.getBalance("CK12345678901234567890")).isEqualByComparingTo(
            new BigDecimal("200.00"));
    }

    @Test
    void testGetBalanceForNonExisitingAccount() {
        assertThatThrownBy(() -> bankAccountService.getBalance("CK67890123456789012345")).isInstanceOf(
            ResponseStatusException.class).hasMessageContaining("Account not found");
    }

    @Test
    void testFindAllByAllAccountTypes() {

        final Collection<Account> accountsByAnyType = bankAccountService.getAllByAccountTypes(null);

        assertThat(accountsByAnyType).hasSize(3);

        assertThat(accountsByAnyType.stream()
            .map(Account::getAccountType)
            .anyMatch(accountType -> accountType == CHECKING))
            .isTrue();
        assertThat(accountsByAnyType.stream()
            .map(Account::getAccountType)
            .anyMatch(accountType -> accountType == SAVINGS))
            .isTrue();
        assertThat(accountsByAnyType.stream()
            .map(Account::getAccountType)
            .anyMatch(accountType -> accountType == PRIVATE_LOAN))
            .isTrue();
    }

    @Test
    void testFindAllBySpecificAccountType() {

        final Collection<Account> accountsByType =
            bankAccountService.getAllByAccountTypes(Collections.singleton(CHECKING));

        assertThat(accountsByType).hasSize(1);
        assertThat(accountsByType.stream()
            .map(Account::getAccountType)
            .allMatch(accountType -> accountType == CHECKING))
            .isTrue();
    }

    @Test
    void testFindTransactionsByIbanWithExistingAccount() {

        final Collection<Transaction> transactions = bankAccountService.getTransactionsByIban(
            "CK12345678901234567890");

        assertThat(transactions).hasSize(1);

        final Transaction transaction = transactions.iterator().next();

        assertThat(transaction.getAmount()).isEqualByComparingTo(new BigDecimal("250.00"));
        assertThat(transaction.getSource()).isNull();
        assertThat(transaction.getTarget()).isEqualTo("CK12345678901234567890");
    }

    @Test
    void testFindNoTransactionsByIbanWithExistingAccount() {

        final Collection<Transaction> transactions = bankAccountService.getTransactionsByIban(
            "PL34567890123456789012");

        assertThat(transactions).hasSize(0);
    }

    @Test
    void testFindTransactionsByIbanWithNonExistingAccount() {

        final Collection<Transaction> transactions = bankAccountService.getTransactionsByIban(
            "CK67890123456789012345");

        assertThat(transactions).hasSize(0);
    }

    @Test
    void testCreateCheckingAccount() {

        final Account account = Account.builder()
            .iban("CK45678901234567890123")
            .accountType(CHECKING)
            .build();

        bankAccountService.createAccount(account, null);

        verify(referenceAccountRepository, never()).save(any());
        verify(accountRepositoryMock, times(1)).save(account);
    }

    @Test
    void testCreateSavingsAccountWithCheckingAccount() {

        final Account account = Account.builder()
            .iban("SV56789012345678901234")
            .accountType(SAVINGS)
            .build();

        bankAccountService.createAccount(account, "CK12345678901234567890");

        verify(referenceAccountRepository, times(1)).save(referenceAccountCaptor.capture());
        verify(accountRepositoryMock, times(1)).save(account);

        final ReferenceAccount referenceAccount = referenceAccountCaptor.getValue();

        assertThat(referenceAccount.getSavingsAccountIban()).isEqualTo("SV56789012345678901234");
        assertThat(referenceAccount.getCheckingAccountIban()).isEqualTo("CK12345678901234567890");
    }

    @Test
    void testCreateSavingsAccountWithExistingIban() {

        final Account account = Account.builder()
            .iban("SV23456789012345678901")
            .accountType(SAVINGS)
            .build();

        assertThatThrownBy(() -> bankAccountService.createAccount(account, "CK12345678901234567890"))
            .isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("An account for this IBAN already exists");
    }

    @Test
    void testCreateSavingsAccountWithoutCheckingAccount() {

        final Account account = Account.builder()
            .iban("SV56789012345678901234")
            .accountType(SAVINGS)
            .build();

        assertThatThrownBy(() -> bankAccountService.createAccount(account, null))
            .isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("SAVINGS account creation require a reference, but none was specified");
    }

    @Test
    void testCreateSavingsAccountWithPrivateLoanAccount() {

        final Account account = Account.builder()
            .iban("SV56789012345678901234")
            .accountType(SAVINGS)
            .build();

        assertThatThrownBy(() -> bankAccountService.createAccount(account, "PL34567890123456789012"))
            .isInstanceOf(ResponseStatusException.class)
            .hasMessageContaining("SAVINGS account creation requires a CHECKING account as reference");
    }

    @Test
    void testChangeLockForExistingAccount() {

        bankAccountService.changeAccountLock("CK12345678901234567890");

        verify(accountRepositoryMock, times(1)).findById("CK12345678901234567890");
        verify(accountRepositoryMock, times(1)).save(accountCaptor.capture());

        assertThat(accountCaptor.getValue().isLocked()).isTrue();
    }
}