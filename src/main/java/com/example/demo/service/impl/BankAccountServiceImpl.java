package com.example.demo.service.impl;

import static com.example.demo.model.Account.AccountType.CHECKING;
import static com.example.demo.model.Account.AccountType.PRIVATE_LOAN;
import static com.example.demo.model.Account.AccountType.SAVINGS;

import com.example.demo.model.Account;
import com.example.demo.model.Account.AccountType;
import com.example.demo.model.ReferenceAccount;
import com.example.demo.model.Transaction;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.ReferenceAccountRepository;
import com.example.demo.repository.TransactionRepository;
import com.example.demo.service.BankAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

@Component
@RequiredArgsConstructor
public class BankAccountServiceImpl implements BankAccountService {

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;
    private final ReferenceAccountRepository referenceAccountRepository;

    /*
    This method will deposit money to an account as specified in the transaction.
    It will fail when the account does not exist.
     */
    @Override
    public Transaction deposit(Transaction transaction) {

        final Account account = getAccount(transaction.getTarget());
        updateBalance(account, transaction.getAmount());

        return transactionRepository.save(transaction);
    }

    /*
    This method will transfer money between two account as specified in the transaction.
    It will fail if either of the accounts don't exist or the restriction from the task are violated.
    */
    @Override
    public Transaction transfer(Transaction transaction) {

        final Account sourceAccount = getAccount(transaction.getSource());
        final Account targetAccount = getAccount(transaction.getTarget());
        final BigDecimal amount = transaction.getAmount();

        try {
            validateTransfer(sourceAccount, targetAccount, amount);
        } catch (final IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }

        updateBalance(sourceAccount, amount.negate());
        updateBalance(targetAccount, amount);

        return transactionRepository.save(transaction);
    }

    /*
    This will return the current balance of an account.
    It will fail when the account does not exist.
     */
    @Override
    public BigDecimal getBalance(String iban) {
        return getAccount(iban).getBalance();
    }

    /*
    This will return all accounts with the possibility to filter by AccountType (CHECKING, etc.)
    It will return an empty Collection if no accounts were found.
     */
    @Override
    public Collection<Account> getAllByAccountTypes(Collection<AccountType> accountTypes) {

        final Iterable<Account> accountIterable;

        if (CollectionUtils.isEmpty(accountTypes)) {
            accountIterable = accountRepository.findAll();
        } else {
            accountIterable = accountRepository.findAccountsByAccountTypeInOrderByCreatedAtDesc(accountTypes);
        }

        final Collection<Account> accounts = new ArrayList<>();
        accountIterable.iterator().forEachRemaining(accounts::add);

        return accounts;
    }

    /*
    This will return all transaction for an IBAN.
    It will return an empty collection if the account doesn't exist or there are no transactions associated with it.
     */
    @Override
    public Collection<Transaction> getTransactionsByIban(String iban) {
        return transactionRepository.findAllbyIbanOrderByTimestampDesc(iban);
    }

    /*
    This will create a new account.
    It is mandatory to include a valid checking account when opening a savings account.
    It will fail also if the specified IBAN is already used.
     */
    @Override
    public void createAccount(Account account, String referenceAccountIban) {

        final String newAccountIban = account.getIban();

        try {
            if (accountRepository.existsById(newAccountIban)) {
                throw new IllegalArgumentException("An account for this IBAN already exists");
            }

            if (account.getAccountType() == SAVINGS) {
                final ReferenceAccount referenceAccount = getValidatedReferenceAccount(newAccountIban,
                    referenceAccountIban
                );
                referenceAccountRepository.save(referenceAccount);
            }
        } catch (final IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }

        accountRepository.save(account);
    }

    /*
    This will toggle the lock on the account on/ off.
    It will fail if the account does not exist.
     */
    @Override
    public Account changeAccountLock(String iban) {

        final Account account = getAccount(iban);
        final Account updatedAccount = account.toBuilder().locked(!account.isLocked()).build();

        return accountRepository.save(updatedAccount);
    }

    /*
    Helper method to get an existing account.
    Will throw an exception if the account doesn't exist.
     */
    private Account getAccount(String iban) {
        return accountRepository.findById(iban)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found"));
    }

    /*
    Helper method that creates a new immutable Account and saves it.
     */
    private void updateBalance(Account account, BigDecimal amount) {

        final Account updatedAccount = account.toBuilder().balance(account.getBalance().add(amount)).build();
        accountRepository.save(updatedAccount);
    }

    /*
    Helper method to validate that the transfer fulfills all requirements.
     */
    private void validateTransfer(Account sourceAccount, Account targetAccount, BigDecimal amount) {

        // technically not a requirement, but to me it made sense to check this condition as well
        if (sourceAccount.getIban().equals(targetAccount.getIban())) {
            throw new IllegalArgumentException("Source and target account cannot be identical");
        }

        // technically not a requirement either
        if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Amount needs to have a positive value");
        }

        if (sourceAccount.getAccountType() == PRIVATE_LOAN) {
            throw new IllegalArgumentException("Withdrawal from private loan account is not possible");
        }

        if (sourceAccount.getAccountType() == SAVINGS) {
            if (!isValidCheckingAccount(sourceAccount, targetAccount)) {
                throw new IllegalArgumentException(
                    "Transfer from savings account is only possible to the designated reference account");
                // technically not a requirement either
            } else if (sourceAccount.getBalance().compareTo(amount) < 0) {
                throw new IllegalArgumentException(
                    "Cannot withdraw more than the current balance from savings account");
            }
        }
    }

    /*
    Helper method that will check whether or not a savings account is correctly linked to a checking account.
    This could be considered redundant as the getValidatedReferenceAccount should be called when opening new
    savings accounts.
     */
    private boolean isValidCheckingAccount(Account sourceAccount, Account targetAccount) {

        if (targetAccount.getAccountType() != CHECKING) {
            return false;
        }

        final ReferenceAccount referenceAccount = referenceAccountRepository.findById(sourceAccount.getIban())
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Reference account not found"));

        return referenceAccount.getCheckingAccountIban().equals(targetAccount.getIban());
    }

    /*
    Helper method that will check if all requirements for opening a savings account are fulfilled.
     */
    private ReferenceAccount getValidatedReferenceAccount(String newAccountIban, String referenceAccountIban) {

        if (referenceAccountIban == null) {
            throw new IllegalArgumentException("SAVINGS account creation require a reference, but none was specified");
        }

        final Account referenceAccount = getAccount(referenceAccountIban);

        if (referenceAccount == null || referenceAccount.getAccountType() != CHECKING) {
            throw new IllegalArgumentException("SAVINGS account creation requires a CHECKING account as reference");
        }
        return new ReferenceAccount(newAccountIban, referenceAccountIban);
    }
}