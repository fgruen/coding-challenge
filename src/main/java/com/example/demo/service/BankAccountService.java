package com.example.demo.service;

import com.example.demo.model.Account;
import com.example.demo.model.Account.AccountType;
import com.example.demo.model.Transaction;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collection;

public interface BankAccountService {

    @Modifying
    @Transactional
    Transaction deposit(Transaction transaction);

    @Modifying
    @Transactional
    Transaction transfer(Transaction transaction);

    BigDecimal getBalance(String iban);

    Collection<Account> getAllByAccountTypes(Collection<AccountType> accountTypes);

    Collection<Transaction> getTransactionsByIban(String iban);

    @Transactional
    void createAccount(Account account, String referenceAccount);

    Account changeAccountLock(String iban);
}
