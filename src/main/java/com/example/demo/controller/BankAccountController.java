package com.example.demo.controller;

import com.example.demo.model.Account;
import com.example.demo.model.Account.AccountType;
import com.example.demo.model.Transaction;
import com.example.demo.service.BankAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/account")
public class BankAccountController {

    private final BankAccountService bankAccountService;

    // Deposit money into a specified bank account
    @PostMapping("/deposit")
    public Transaction deposit(@Valid @RequestBody Transaction transaction) {
        return bankAccountService.deposit(transaction);
    }

    // Transfer some money across two bank accounts
    @PostMapping("/transfer")
    public Transaction transfer(@Valid @RequestBody Transaction transaction) {
        return bankAccountService.transfer(transaction);
    }

    // Show current balance of the specific bank account
    @GetMapping("/{iban}/balance")
    public BigDecimal getBalance(@PathVariable String iban) {
        return bankAccountService.getBalance(iban);
    }

    // Filter accounts by account type
    @GetMapping("/all")
    public Collection<Account> getAllAccountsByAccountTypes(
        @RequestParam(required = false) Collection<AccountType> filter) {
        return bankAccountService.getAllByAccountTypes(filter);
    }

    // Show a transaction history
    @GetMapping("/{iban}/transactions")
    public Collection<Transaction> getAllTransactionsByIban(@PathVariable String iban) {
        return bankAccountService.getTransactionsByIban(iban);
    }

    // Bonus - account creation
    @ResponseStatus(HttpStatus.CREATED)
    @PutMapping(value = {"/createAccount", "{referenceAccount}/createAccount"})
    public void createAccount(@Valid @RequestBody Account account, @PathVariable Optional<String> referenceAccount) {
        bankAccountService.createAccount(account, referenceAccount.orElse(null));
    }

    // Bonus - account locking
    @PatchMapping("/{iban}/changeLock")
    public Account changeAccountLock(@PathVariable String iban) {
        return bankAccountService.changeAccountLock(iban);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getFieldErrors().forEach(error ->
            errors.put(error.getField(), error.getDefaultMessage()));

        return errors;
    }
}
