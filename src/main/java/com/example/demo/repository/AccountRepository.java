package com.example.demo.repository;

import com.example.demo.model.Account;
import com.example.demo.model.Account.AccountType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface AccountRepository extends CrudRepository<Account, String> {

    Iterable<Account> findAccountsByAccountTypeInOrderByCreatedAtDesc(Collection<AccountType> accountTypes);
}
