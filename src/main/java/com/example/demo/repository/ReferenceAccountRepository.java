package com.example.demo.repository;

import com.example.demo.model.ReferenceAccount;
import org.springframework.data.repository.CrudRepository;

public interface ReferenceAccountRepository extends CrudRepository<ReferenceAccount, String> {
}
