package com.example.demo.repository;

import com.example.demo.model.Transaction;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    @Query("SELECT * FROM transaction WHERE source = :iban OR target = :iban ORDER BY timestamp DESC")
    Collection<Transaction> findAllbyIbanOrderByTimestampDesc(@Param("iban") String iban);
}
