package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jdbc.repository.config.EnableJdbcAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableJdbcAuditing
@EnableTransactionManagement
public class BankAccountToyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankAccountToyApplication.class, args);
    }
}
