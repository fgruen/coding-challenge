package com.example.demo.model;

import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;

@Value
public class ReferenceAccount implements Persistable<String> {

    @Id
    String savingsAccountIban;

    String checkingAccountIban;

    @Override
    public String getId() {
        return savingsAccountIban;
    }

    @Override
    public boolean isNew() {
        return true;
    }
}
