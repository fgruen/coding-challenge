package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Builder(toBuilder = true)
@Validated
@JsonDeserialize(builder = Account.AccountBuilder.class)
public class Account implements Persistable<String> {

    public enum AccountType {
        CHECKING,
        SAVINGS,
        PRIVATE_LOAN
    }

    @Id
    @JsonProperty
    @Size(min = 22, max = 22, message = "The IBAN needs to be 22 characters long")
    private final String iban;

    @JsonProperty
    @NotNull(message = "AccountType cannot be empty")
    private final AccountType accountType;

    @JsonProperty
    @Builder.Default
    private final BigDecimal balance = BigDecimal.ZERO;

    private final boolean locked;

    @CreatedDate
    private final LocalDateTime createdAt;

    @LastModifiedDate
    private final LocalDateTime lastTransaction;

    @JsonPOJOBuilder
    public static final class AccountBuilder {

    }

    @Override
    public String getId() {
        return iban;
    }

    @Override
    public boolean isNew() {
        return createdAt == null;
    }
}
