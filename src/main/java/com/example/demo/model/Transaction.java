package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;

@Getter
@Builder
@Validated
@JsonDeserialize(builder = Transaction.TransactionBuilder.class)
public class Transaction {

    @Id
    private final Long id;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String source;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String target;

    @NotNull(message = "The amount for the transaction cannot be empty")
    @JsonProperty
    private final BigDecimal amount;

    @CreatedDate
    private final LocalDateTime timestamp;

    @JsonPOJOBuilder
    public static final class TransactionBuilder {

    }
}
