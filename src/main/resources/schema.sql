-- ************************************** `account` ---

CREATE TABLE account
(
    iban                VARCHAR(22) NOT NULL PRIMARY KEY COMMENT 'iban',
    account_type        ENUM('CHECKING', 'SAVINGS', 'PRIVATE_LOAN') NOT NULL COMMENT 'type of account',
    balance             DECIMAL NOT NULL DEFAULT 0.0 COMMENT 'current balance (in Euro)',
    created_at          TIMESTAMP NOT NULL DEFAULT '2021-03-01 18:00:00' COMMENT 'time of creation',
    last_transaction    TIMESTAMP NOT NULL DEFAULT '2021-03-01 18:00:00' COMMENT 'time of last transaction',
    locked              BOOLEAN NOT NULL DEFAULT false COMMENT 'is the account locked?'
);
-- ************************************** `transaction` ---

CREATE TABLE transaction
(
    id          INT NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'transaction id',
    source      VARCHAR(22) COMMENT 'iban of source account (empty for deposits)',
    target      VARCHAR(22) COMMENT 'iban of target account (empty for withdrawals)',
    amount      DECIMAL NOT NULL COMMENT 'amount of transaction (in Euro)',
    timestamp   TIMESTAMP DEFAULT '2021-03-01 18:00:00' COMMENT 'time of transaction'
);

-- ************************************** `reference_account` ---

CREATE TABLE reference_account
(
    savings_account_iban    VARCHAR(22) NOT NULL PRIMARY KEY COMMENT 'iban for savings account',
    checking_account_iban   VARCHAR(22) NOT NULL COMMENT 'iban for checking account',
    UNIQUE (savings_account_iban, checking_account_iban)
);