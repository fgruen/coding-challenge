INSERT INTO account (IBAN, ACCOUNT_TYPE, BALANCE) VALUES ( 'CK12345678901234567890', 'CHECKING', 200.00 );
INSERT INTO account (IBAN, ACCOUNT_TYPE, BALANCE) VALUES ( 'SV23456789012345678901', 'SAVINGS', 100.00 );
INSERT INTO account (IBAN, ACCOUNT_TYPE) VALUES ( 'PL34567890123456789012', 'PRIVATE_LOAN' );

INSERT INTO reference_account VALUES ( 'SV23456789012345678901', 'CK12345678901234567890' );