Thanks for checking out this small demo project!

For starting the application, you could also use the gradle wrapper

`./gradlew bootRun`

I have included a schemas and values for an in-memory database, but feel free to swap the datasource.
One example request for each of the required calls have been included as well.

I have included complete test coverage for the BankAccountService. These can be run via
`./gradlew test`

### Caveats
I have tried to make all used models immutable. This leads to a run-time warning
```WARNING: An illegal reflective access operation has occurred```

However, it does not impact the functionality of the application at all.